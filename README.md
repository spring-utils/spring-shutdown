# Spring graceful shutdown

It is not god-given that a spring-applications finishes graceful!

I cover the most used kinds of parallel processes here to show how to finish them nice:

- async events
- multi threading
- rest calls
- scheduler

The trick is to define a thread-pool for each parallel process.

Tip: for some IDEs (Eclipse) it is necessary to install the spring-plugin, because if you run only the main-class the IDE does stop the application hard no matter if you define  thread-pools or not. 