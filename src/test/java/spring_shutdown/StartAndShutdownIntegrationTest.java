package spring_shutdown;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StartAndShutdownIntegrationTest {

	@Test
	public void run_application() throws Exception {
		Thread.sleep(1_000);
	}
}
