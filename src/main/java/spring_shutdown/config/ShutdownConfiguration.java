package spring_shutdown.config;

import org.springframework.aop.interceptor.AsyncExecutionAspectSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableAsync
@EnableScheduling
@Configuration
@Getter
public class ShutdownConfiguration implements SchedulingConfigurer {

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setTaskScheduler(createSchedulerThreadPool());
	}

	@Bean(name = AsyncExecutionAspectSupport.DEFAULT_TASK_EXECUTOR_BEAN_NAME)
	public TaskExecutor threadPoolTaskExecutor() {
		return createThreadPool("Event-Pool");
	}

	@Bean // only for scheduler
	public TaskScheduler createSchedulerThreadPool() {
		return createThreadPool("Sch-Pool");
	}

	@Bean(name = "rest-thread-pool")
	public ThreadPoolTaskScheduler createRESTThreadPool() {
		return createThreadPool("REST-Pool");
	}

	private ThreadPoolTaskScheduler createThreadPool(String name) {
		ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
		scheduler.setThreadNamePrefix(name);
		scheduler.setErrorHandler(exception -> log.error("Exception in thread.", exception));
		scheduler.setPoolSize(3);
		scheduler.setWaitForTasksToCompleteOnShutdown(true);
		scheduler.setAwaitTerminationSeconds(30_000);
		return scheduler;
	}
}