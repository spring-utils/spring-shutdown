package spring_shutdown.controller;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ShutdownController {

	@Autowired
	@Qualifier("rest-thread-pool")
	private ThreadPoolTaskScheduler threadPool;

	@RequestMapping("/fast")
	public String fast() {
		log.info("~~~~~~~ REST IN ~~~~~~~~~~~~ " + threadPool.getActiveCount());

		CompletableFuture.supplyAsync(this::processRequest, threadPool);

		log.info("~~~~~~~ REST OUT ~~~~~~~~~~~~ " + threadPool.getActiveCount());

		return "Hello";
	}

	@RequestMapping("/slow")
	public CompletableFuture<String> slow() {
		log.info("~~~~~~~ REST IN ~~~~~~~~~~~~ " + threadPool.getActiveCount());

		CompletableFuture<String> supplyAsync = CompletableFuture.supplyAsync(this::processRequest, threadPool);

		log.info("~~~~~~~ REST OUT ~~~~~~~~~~~~ " + threadPool.getActiveCount());

		return supplyAsync;
	}

	private String processRequest() {
		try {
			log.info("~~~~~~~ REST START WAIT ~~~~~~~~~~~~ " + threadPool.getActiveCount());
			Thread.sleep(15_000);
			log.info("~~~~~~~ REST END WAIT ~~~~~~~~~~~~ " + threadPool.getActiveCount());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "Hello";
	}
}
