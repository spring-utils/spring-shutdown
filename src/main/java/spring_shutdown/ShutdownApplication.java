package spring_shutdown;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class ShutdownApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(ShutdownApplication.class).run(args);
	}
}
