package spring_shutdown.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ShutdownScheduler {

	@Scheduled(fixedRate = 3_000)
	public void doSchedulerStuff() {
		try {
			log.info("ShutdownScheduler start!");
			Thread.sleep(8_000L);
			log.info("ShutdownScheduler stop!");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
