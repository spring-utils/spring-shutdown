package spring_shutdown.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import spring_shutdown.eventing.ShutdownEvent;

@Slf4j
@Component
public class EventSendScheduler {

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	private int counter = 0;

	@Scheduled(fixedRate = 1_000)
	public void doSchedulerStuff() {
		counter++;
		log.info("+++++++++ Send event: " + counter);
		eventPublisher.publishEvent(new ShutdownEvent("Id: " + counter));
	}
}
