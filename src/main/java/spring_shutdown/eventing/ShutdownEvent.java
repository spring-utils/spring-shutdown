package spring_shutdown.eventing;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ShutdownEvent {
	private String message;
}
