package spring_shutdown.eventing;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ShutdownEventListener {

	@Async
	@EventListener
	public void receiveEvent(ShutdownEvent event) {

		log.info("++++++ Start receive event:" + event.getMessage());
		try {
			Thread.sleep(5_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			log.debug("", e);
		}
		log.info("++++++ End receive event:" + event.getMessage());
	}
}
